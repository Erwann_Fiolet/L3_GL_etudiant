#include "Fibo.hpp"
#include <iostream>

int main() {
    try {
		for (int i=0; i<50; i++)
			std::cout << fibo(i) << std::endl;
    }
    catch (std::string const& chaine){
        std::cout << chaine << std::endl;
    }
    return 0;
}

