#include "Fibo.hpp"
#include <assert.h>
#include <iostream>

int fibo(int n, int f0, int f1) {
	if (f1<0) throw std::string("Négatif");
	if (f0>f1) throw std::string("Probleme");
    return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}

