#include "Fibo.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo) { };

TEST(GroupFibo, Fibo_firstValue)  {
    CHECK_EQUAL(fibo(1), 1);
    CHECK_EQUAL(fibo(2), 1);
    CHECK_EQUAL(fibo(3), 2);
    CHECK_EQUAL(fibo(4), 3);
    CHECK_EQUAL(fibo(5), 5);
}
