#include "Spline.hpp"
#include <iostream>

using namespace std;

int main() {

    // begin

    // create spline
    Spline spline;

    // add keys
    double t;
    Vec2 P;
	while (cin >> t and cin >> P.x_ and cin >> P.y_) {
		try {
			spline.addKey(t, P);
		} catch (std::string const& chaine)  {
			std::cout << chaine << std::endl;
		}
	}

    // compute values
    double deltaT = (spline.getEndTime() - spline.getStartTime()) / 100.0;
	for (double t=spline.getStartTime(); t<spline.getEndTime(); t+=deltaT) {
		try {
			Vec2 Pt = spline.getValue(t);
			cout << t << " " << Pt.x_ << " " << Pt.y_ << "\n";
		} catch (std::string const& chaine)  {
			std::cout << chaine << std::endl;
		}
	}

    // end

    return 0;
}

