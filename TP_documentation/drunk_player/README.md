## Drunk_player

### Description

Drunk_player est un système de vidéos qui a trop bu.

Drunk_player est composé :

- d'une biblio
- d'un programme graphique
- d'un programme console


### Dépendance

- OpenCV
- Boost


### Compilation


**mkdir** build
**cd** build
cmake ..
**make**


### Utilisation

