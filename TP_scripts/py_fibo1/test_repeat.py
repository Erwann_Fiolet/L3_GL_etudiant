#! /usr/bin/env python3

import repeat
import fibo

def print_fibo(n):
	print(fibo.fiboIterative(n))
	
if __name__ == '__main__':
#	print_fibo(3)
	repeat.repeatN(10,print_fibo)
