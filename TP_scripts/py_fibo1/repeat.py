#! /usr/bin/env python3

import fibo

def repeatN(n,f):
	for i in range(n):
		f(i)
	#print([f(i) for i in range(n)])
		

if __name__ == '__main__':
	repeatN(10,fibo.fiboIterative)
