
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import fibo
import sinus
import numpy
		

xs = range(10)
ys = [fibo.fiboIterative(x) for x in xs]
plt.plot(xs, ys) 
plt.xlabel('x')
plt.ylabel('fiboIterative(x)')
plt.grid()
plt.savefig('plot_fibo.png')
plt.clf()

xs = numpy.arange(0.0,1.0,0.01)
ys = [sinus.sinus(2,0.25,x) for x in xs]
plt.plot(xs, ys)
plt.xlabel('x')
plt.ylabel('sinus(2,0.25,x)')
plt.grid()
plt.savefig('plot_sinus.png')
plt.clf()




