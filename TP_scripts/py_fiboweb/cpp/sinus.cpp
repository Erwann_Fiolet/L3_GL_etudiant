#include <iostream>
#include <math.h>

#define PI 3.14159265

double sinus(int a,int b,double x){
	double result;
	result = sin(2.0*PI*(a*x+b));
	return result;
}

#include <pybind11/pybind11.h>
PYBIND11_PLUGIN(sinus) {
    pybind11::module m("sinus");
    m.def("sinus", &sinus);
    return m.ptr();
}
